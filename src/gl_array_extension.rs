use std::ffi::c_void;

pub trait CPtr {
    fn size_of(&self) -> isize;
    fn as_const_ptr(&self) -> *const c_void;
}

impl<T> CPtr for [T] {
    fn size_of(&self) -> isize {
        (self.len() * std::mem::size_of::<T>()) as isize
    }

    fn as_const_ptr(&self) -> *const c_void {
        self.as_ptr() as *const c_void
    }
}
