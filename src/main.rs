use gl;
use sdl2::{self, event::WindowEvent};

use std::ffi::CString;

use crate::gl_context::GLContextExtensions;

pub mod gl_array_extension;
pub mod gl_context;

const WIDTH: u32 = 1920;
const HEIGHT: u32 = 1080;

fn main() {
    let _sdl = sdl2::init().unwrap();
    let video_subsystem = _sdl.video().unwrap();

    //defining min OpenGL version to use
    let gl_attr = video_subsystem.gl_attr();
    gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    gl_attr.set_context_version(4, 5);

    let window = video_subsystem
        .window("Game", WIDTH, HEIGHT)
        .opengl()
        .resizable()
        .build()
        .unwrap();

    let gl_context = window.gl_create_context().unwrap();
    let _gl =
        gl::load_with(|s| video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void);

    unsafe {
        gl::Viewport(0, 0, window.size().0 as i32, window.size().1 as i32);
        gl::ClearColor(0.3, 0.3, 0.5, 1.0);
    }
    //square defining
    let vert_shader_sqr = gl_context::Shader::from_vert_source(
        &CString::new(include_str!("./shaders/square.vert")).unwrap(),
    )
    .unwrap();
    let frag_shader_sqr = gl_context::Shader::from_frag_source(
        &CString::new(include_str!("./shaders/square.frag")).unwrap(),
    )
    .unwrap();
    let shader_program_sqr =
        gl_context::Program::from_shaders(&[vert_shader_sqr, frag_shader_sqr]).unwrap();

    let vertices_sqr = [
        // positions
        0.5f32, 0.5, 0.0, // top right
        0.5, -0.5, 0.0, // bottom right
        -0.5, -0.5, 0.0, // bottom left
        -0.5, 0.5, 0.0, // top left
    ];

    let indicies = [0u32, 1, 3, 1, 2, 3];

    let vertex_buffer_object_sqr = gl_context.new_buffer(gl::ARRAY_BUFFER);
    let element_buffer_object_sqr = gl_context.new_buffer(gl::ELEMENT_ARRAY_BUFFER);

    vertex_buffer_object_sqr.set_data(&vertices_sqr, gl::STATIC_DRAW);
    element_buffer_object_sqr.set_data(&indicies, gl::STATIC_DRAW);

    let vertex_array_object_sqr = gl_context.new_VAO();
    vertex_array_object_sqr.bind_buffer_only(&element_buffer_object_sqr);
    vertex_array_object_sqr.bind_buffer_data(
        &vertex_buffer_object_sqr,
        0,
        3,
        gl::FLOAT,
        gl::FALSE,
        12,
        std::ptr::null(),
    );

    //colored triangle defining
    let vert_shader_trg = gl_context::Shader::from_vert_source(
        &CString::new(include_str!("./shaders/triangle.vert")).unwrap(),
    )
    .unwrap();
    let frag_shader_trg = gl_context::Shader::from_frag_source(
        &CString::new(include_str!("./shaders/triangle.frag")).unwrap(),
    )
    .unwrap();
    let shader_program_trg =
        gl_context::Program::from_shaders(&[vert_shader_trg, frag_shader_trg]).unwrap();

    let vertices_trg = [
        // positions    // colors
        0.5f32, -0.5, 0.0, // bottom right
        -0.5, -0.5, 0.0, // bottom left
        0.0, 0.5, 0.0, // top
    ];
    let colors_trg = [1.0f32, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0];
    let indicies_trg = [0u32, 1, 2];

    let element_buffer_object_trg = gl_context.new_buffer(gl::ELEMENT_ARRAY_BUFFER);
    element_buffer_object_trg.set_data(&indicies_trg, gl::STATIC_DRAW);

    let vertex_buffer_object_color_trg = gl_context.new_buffer(gl::ARRAY_BUFFER);
    vertex_buffer_object_color_trg.set_data(&colors_trg, gl::STATIC_DRAW);

    let vertex_buffer_object_trg = gl_context.new_buffer(gl::ARRAY_BUFFER);
    vertex_buffer_object_trg.set_data(&vertices_trg, gl::STATIC_DRAW);

    let vertex_array_object_trg = gl_context.new_VAO();
    vertex_array_object_trg.bind_buffer_only(&element_buffer_object_trg);
    vertex_array_object_trg.bind_buffer_data(
        &vertex_buffer_object_trg,
        0,
        3,
        gl::FLOAT,
        gl::FALSE,
        12,
        std::ptr::null(),
    );

    vertex_array_object_trg.bind_buffer_data(
        &vertex_buffer_object_color_trg,
        1,
        3,
        gl::FLOAT,
        gl::FALSE,
        12,
        std::ptr::null(),
    );

    let mut sqr = false; //square
    let mut trg = false;

    let mut event_pump = _sdl.event_pump().unwrap();
    'main: loop {
        for _event in event_pump.poll_iter() {
            match _event {
                sdl2::event::Event::Quit { timestamp } => {
                    println!("{} saniye sürdü.", timestamp as f32 / 1000f32);
                    break 'main;
                }
                sdl2::event::Event::Window { win_event, .. } => match win_event {
                    WindowEvent::SizeChanged(w, h) => unsafe {
                        gl::Viewport(0, 0, w as i32, h as i32);
                    },
                    _ => {}
                },
                sdl2::event::Event::KeyUp {
                    timestamp,
                    window_id,
                    keycode,
                    scancode,
                    keymod,
                    repeat,
                } => match keycode {
                    Some(key) => {
                        println!("Key Up: {}", key);
                        match key {
                            sdl2::keyboard::Keycode::A => {
                                trg = !trg;
                            }
                            sdl2::keyboard::Keycode::S => {
                                sqr = !sqr;
                            }
                            _ => {}
                        }
                    }
                    None => {}
                },
                _ => {}
            }
        }
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
        if sqr {
            shader_program_sqr.use_self();
            unsafe {
                gl::BindVertexArray(vertex_array_object_sqr.id);
                gl::DrawElements(
                    gl::TRIANGLES, // mode
                    6,             // starting index in the enabled arrays
                    gl::UNSIGNED_INT,
                    std::ptr::null(), // number of indices to be rendered
                );
                // gl::DrawArrays(gl::TRIANGLES, 0, 3);
                gl::BindVertexArray(0);
            }
        }
        if trg {
            shader_program_trg.use_self();
            unsafe {
                gl::BindVertexArray(vertex_array_object_trg.id);
                gl::DrawElements(
                    gl::TRIANGLES, // mode             // starting index in the enabled arrays
                    3,             // number of indices to be rendered
                    gl::UNSIGNED_INT,
                    std::ptr::null(),
                );
                gl::BindVertexArray(0);
            }
        }

        window.gl_swap_window();
    }
}
